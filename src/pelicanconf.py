#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Ussisõnad'
SITENAME = 'SampleBlog'
SITEURL = 'https://zcribe.github.io/PelicanBlogExample/'

PATH = 'content'

TIMEZONE = 'Europe/Tallinn'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Postimees', 'http://postimees.ee/'),
         ('ERR', 'http://err.ee'),)

# Social widget
SOCIAL = (('Facebook', '#'),
          ('Twitter', '#'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = 'pelican-semantic-ui'

# Google analüütika kood
GOOGLE_ANALYTICS = ''
# Kommenteerimise integratsioon
DISQUS_SITENAME = ''
# Tervitus
WELCOME_MESSAGE = 'Tere saidilie'

MENU_LINKS = (('Pea', 'http://err.ee'),
              ('Jalad', 'http://err.ee'),
              ('Kõht', 'http://err.ee'),)
LOGOPATH = ''